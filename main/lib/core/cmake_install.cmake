# Install script for directory: /home/calice/eudaq/main/lib/core

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/calice/eudaq")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeudaq_core.so.2.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeudaq_core.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "$ORIGIN/../lib:$ORIGIN/../extern/lib")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/calice/eudaq/main/lib/core/libeudaq_core.so.2.0"
    "/home/calice/eudaq/main/lib/core/libeudaq_core.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeudaq_core.so.2.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libeudaq_core.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "::::::::::::::::::::::::::::::::::::"
           NEW_RPATH "$ORIGIN/../lib:$ORIGIN/../extern/lib")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eudaq" TYPE FILE FILES
    "/home/calice/eudaq/main/lib/core/include/eudaq/BufferSerializer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/CommandReceiver.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Configuration.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/DataCollector.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/DataConverter.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/DataReceiver.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/DataSender.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Deserializer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Documentation.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Event.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Exception.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Factory.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/FileDeserializer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/FileNamer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/FileReader.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/FileSerializer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/FileWriter.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/LogCollector.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/LogMessage.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/LogSender.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Logger.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/ModuleManager.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Monitor.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/OptionParser.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Platform.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Processor.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Producer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/RawEvent.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/RunControl.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Serializable.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Serializer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/StandardEvent.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/StandardPlane.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Status.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/StdEventConverter.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Time.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportBase.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportClient.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportNULL.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportServer.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportTCP.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportTCP_POSIX.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/TransportTCP_WIN32.hh"
    "/home/calice/eudaq/main/lib/core/include/eudaq/Utils.hh"
    )
endif()

