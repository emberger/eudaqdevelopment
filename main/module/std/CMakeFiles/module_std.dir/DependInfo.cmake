# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/calice/eudaq/main/module/std/src/DirectSaveDataCollector.cc" "/home/calice/eudaq/main/module/std/CMakeFiles/module_std.dir/src/DirectSaveDataCollector.cc.o"
  "/home/calice/eudaq/main/module/std/src/EventnumberSyncDataCollector.cc" "/home/calice/eudaq/main/module/std/CMakeFiles/module_std.dir/src/EventnumberSyncDataCollector.cc.o"
  "/home/calice/eudaq/main/module/std/src/StdRunControl.cc" "/home/calice/eudaq/main/module/std/CMakeFiles/module_std.dir/src/StdRunControl.cc.o"
  "/home/calice/eudaq/main/module/std/src/SyncByEventnumberPS.cc" "/home/calice/eudaq/main/module/std/CMakeFiles/module_std.dir/src/SyncByEventnumberPS.cc.o"
  "/home/calice/eudaq/main/module/std/src/SyncByTimestampPS.cc" "/home/calice/eudaq/main/module/std/CMakeFiles/module_std.dir/src/SyncByTimestampPS.cc.o"
  "/home/calice/eudaq/main/module/std/src/TimestampSyncDataCollector.cc" "/home/calice/eudaq/main/module/std/CMakeFiles/module_std.dir/src/TimestampSyncDataCollector.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EUDAQ_FUNC=__PRETTY_FUNCTION__ "
  "EUDAQ_PLATFORM=PF_LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "main/lib/core/include"
  "extern/include"
  "include"
  "main/lib/core/include/eudaq"
  "main/lib/lcio/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/calice/eudaq/main/lib/core/CMakeFiles/core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
