/********************************************************************************
** Form generated from reading UI file 'LogDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGDIALOG_H
#define UI_LOGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_dlgLogMessage
{
public:
    QVBoxLayout *verticalLayout;
    QTreeWidget *treeLogMessage;
    QDialogButtonBox *btnClose;

    void setupUi(QDialog *dlgLogMessage)
    {
        if (dlgLogMessage->objectName().isEmpty())
            dlgLogMessage->setObjectName(QStringLiteral("dlgLogMessage"));
        dlgLogMessage->resize(636, 304);
        verticalLayout = new QVBoxLayout(dlgLogMessage);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        treeLogMessage = new QTreeWidget(dlgLogMessage);
        treeLogMessage->setObjectName(QStringLiteral("treeLogMessage"));

        verticalLayout->addWidget(treeLogMessage);

        btnClose = new QDialogButtonBox(dlgLogMessage);
        btnClose->setObjectName(QStringLiteral("btnClose"));
        btnClose->setOrientation(Qt::Horizontal);
        btnClose->setStandardButtons(QDialogButtonBox::Close);
        btnClose->setCenterButtons(true);

        verticalLayout->addWidget(btnClose);


        retranslateUi(dlgLogMessage);
        QObject::connect(btnClose, SIGNAL(accepted()), dlgLogMessage, SLOT(accept()));
        QObject::connect(btnClose, SIGNAL(rejected()), dlgLogMessage, SLOT(reject()));

        QMetaObject::connectSlotsByName(dlgLogMessage);
    } // setupUi

    void retranslateUi(QDialog *dlgLogMessage)
    {
        dlgLogMessage->setWindowTitle(QApplication::translate("dlgLogMessage", "Log Message", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeLogMessage->headerItem();
        ___qtreewidgetitem->setText(2, QApplication::translate("dlgLogMessage", "Short", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("dlgLogMessage", "Full", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("dlgLogMessage", "Name", 0));
    } // retranslateUi

};

namespace Ui {
    class dlgLogMessage: public Ui_dlgLogMessage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGDIALOG_H
