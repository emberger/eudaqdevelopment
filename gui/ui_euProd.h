/********************************************************************************
** Form generated from reading UI file 'euProd.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EUPROD_H
#define UI_EUPROD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_wndProd
{
public:
    QWidget *centralwidget;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout;
    QGroupBox *grpData;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QRadioButton *radFile;
    QLineEdit *txtFile;
    QRadioButton *radCustom;
    QGridLayout *gridLayout;
    QLabel *lblWidth;
    QSpinBox *spnWidth;
    QLabel *lblProfile;
    QLabel *lblX;
    QLabel *lblHeight;
    QSpinBox *spnHeight;
    QLabel *lblRadius;
    QLabel *lblY;
    QComboBox *comboBox;
    QDoubleSpinBox *spnY;
    QDoubleSpinBox *spnX;
    QDoubleSpinBox *spnRadius;
    QGridLayout *gridLayout1;
    QDoubleSpinBox *spnNoise;
    QDoubleSpinBox *spnSparsify;
    QCheckBox *chkPed;
    QCheckBox *chkSparsify;
    QDoubleSpinBox *spnPed;
    QCheckBox *chkNoise;
    QCheckBox *chkCMmean;
    QDoubleSpinBox *spnPulse;
    QCheckBox *chkCMstdev;
    QDoubleSpinBox *spnCMmean;
    QDoubleSpinBox *spnCMstdev;
    QCheckBox *chkPulses;
    QGroupBox *grpTriggers;
    QHBoxLayout *hboxLayout2;
    QVBoxLayout *vboxLayout2;
    QRadioButton *radManual;
    QRadioButton *radFixed;
    QRadioButton *radPoisson;
    QVBoxLayout *vboxLayout3;
    QGridLayout *gridLayout2;
    QDoubleSpinBox *spnFreq;
    QDoubleSpinBox *spnPeriod;
    QLabel *lblFreq;
    QLabel *lblPeriod;
    QPushButton *btnTrigger;
    QVBoxLayout *vboxLayout4;
    QLabel *lblPreview;
    QGraphicsView *grphPreview;
    QGroupBox *grpStatus;
    QVBoxLayout *vboxLayout5;
    QHBoxLayout *hboxLayout3;
    QLineEdit *txtStatus;
    QComboBox *cmbStatus;
    QLabel *lblStatus;
    QFrame *line;
    QGridLayout *gridLayout3;
    QLineEdit *txtDataColl;
    QLineEdit *txtState;
    QLabel *lblRun;
    QLabel *lblState;
    QLabel *lblConfig;
    QLabel *lblDataColl;
    QLineEdit *txtConfig;
    QLabel *lblEvent;
    QLineEdit *txtRun;
    QLineEdit *txtEvent;
    QPushButton *btnQuit;
    QMenuBar *menubar;

    void setupUi(QMainWindow *wndProd)
    {
        if (wndProd->objectName().isEmpty())
            wndProd->setObjectName(QStringLiteral("wndProd"));
        wndProd->resize(705, 542);
        centralwidget = new QWidget(wndProd);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        hboxLayout = new QHBoxLayout(centralwidget);
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        vboxLayout = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        grpData = new QGroupBox(centralwidget);
        grpData->setObjectName(QStringLiteral("grpData"));
        vboxLayout1 = new QVBoxLayout(grpData);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(0);
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        radFile = new QRadioButton(grpData);
        radFile->setObjectName(QStringLiteral("radFile"));

        hboxLayout1->addWidget(radFile);

        txtFile = new QLineEdit(grpData);
        txtFile->setObjectName(QStringLiteral("txtFile"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txtFile->sizePolicy().hasHeightForWidth());
        txtFile->setSizePolicy(sizePolicy);
        txtFile->setMinimumSize(QSize(30, 0));

        hboxLayout1->addWidget(txtFile);


        vboxLayout1->addLayout(hboxLayout1);

        radCustom = new QRadioButton(grpData);
        radCustom->setObjectName(QStringLiteral("radCustom"));
        radCustom->setChecked(true);

        vboxLayout1->addWidget(radCustom);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lblWidth = new QLabel(grpData);
        lblWidth->setObjectName(QStringLiteral("lblWidth"));

        gridLayout->addWidget(lblWidth, 0, 0, 1, 1);

        spnWidth = new QSpinBox(grpData);
        spnWidth->setObjectName(QStringLiteral("spnWidth"));
        sizePolicy.setHeightForWidth(spnWidth->sizePolicy().hasHeightForWidth());
        spnWidth->setSizePolicy(sizePolicy);
        spnWidth->setMinimumSize(QSize(50, 0));
        spnWidth->setFrame(false);
        spnWidth->setAlignment(Qt::AlignRight);
        spnWidth->setMaximum(65536);
        spnWidth->setValue(512);

        gridLayout->addWidget(spnWidth, 0, 1, 1, 1);

        lblProfile = new QLabel(grpData);
        lblProfile->setObjectName(QStringLiteral("lblProfile"));

        gridLayout->addWidget(lblProfile, 1, 0, 1, 1);

        lblX = new QLabel(grpData);
        lblX->setObjectName(QStringLiteral("lblX"));

        gridLayout->addWidget(lblX, 2, 0, 1, 1);

        lblHeight = new QLabel(grpData);
        lblHeight->setObjectName(QStringLiteral("lblHeight"));

        gridLayout->addWidget(lblHeight, 0, 2, 1, 1);

        spnHeight = new QSpinBox(grpData);
        spnHeight->setObjectName(QStringLiteral("spnHeight"));
        sizePolicy.setHeightForWidth(spnHeight->sizePolicy().hasHeightForWidth());
        spnHeight->setSizePolicy(sizePolicy);
        spnHeight->setMinimumSize(QSize(60, 0));
        spnHeight->setFrame(false);
        spnHeight->setAlignment(Qt::AlignRight);
        spnHeight->setMaximum(65536);
        spnHeight->setValue(512);

        gridLayout->addWidget(spnHeight, 0, 3, 1, 1);

        lblRadius = new QLabel(grpData);
        lblRadius->setObjectName(QStringLiteral("lblRadius"));

        gridLayout->addWidget(lblRadius, 1, 2, 1, 1);

        lblY = new QLabel(grpData);
        lblY->setObjectName(QStringLiteral("lblY"));

        gridLayout->addWidget(lblY, 2, 2, 1, 1);

        comboBox = new QComboBox(grpData);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        gridLayout->addWidget(comboBox, 1, 1, 1, 1);

        spnY = new QDoubleSpinBox(grpData);
        spnY->setObjectName(QStringLiteral("spnY"));
        spnY->setFrame(false);
        spnY->setDecimals(1);
        spnY->setMaximum(65536);
        spnY->setValue(256);

        gridLayout->addWidget(spnY, 2, 3, 1, 1);

        spnX = new QDoubleSpinBox(grpData);
        spnX->setObjectName(QStringLiteral("spnX"));
        spnX->setFrame(false);
        spnX->setAlignment(Qt::AlignRight);
        spnX->setDecimals(1);
        spnX->setMaximum(65536);
        spnX->setValue(256);

        gridLayout->addWidget(spnX, 2, 1, 1, 1);

        spnRadius = new QDoubleSpinBox(grpData);
        spnRadius->setObjectName(QStringLiteral("spnRadius"));
        spnRadius->setFrame(false);
        spnRadius->setDecimals(1);
        spnRadius->setMaximum(65536);
        spnRadius->setValue(100);

        gridLayout->addWidget(spnRadius, 1, 3, 1, 1);


        vboxLayout1->addLayout(gridLayout);

        gridLayout1 = new QGridLayout();
        gridLayout1->setSpacing(0);
        gridLayout1->setContentsMargins(0, 0, 0, 0);
        gridLayout1->setObjectName(QStringLiteral("gridLayout1"));
        spnNoise = new QDoubleSpinBox(grpData);
        spnNoise->setObjectName(QStringLiteral("spnNoise"));
        sizePolicy.setHeightForWidth(spnNoise->sizePolicy().hasHeightForWidth());
        spnNoise->setSizePolicy(sizePolicy);
        spnNoise->setMinimumSize(QSize(30, 0));
        spnNoise->setFrame(false);
        spnNoise->setAlignment(Qt::AlignRight);
        spnNoise->setDecimals(1);
        spnNoise->setMaximum(65536);
        spnNoise->setValue(4);

        gridLayout1->addWidget(spnNoise, 3, 1, 1, 1);

        spnSparsify = new QDoubleSpinBox(grpData);
        spnSparsify->setObjectName(QStringLiteral("spnSparsify"));
        sizePolicy.setHeightForWidth(spnSparsify->sizePolicy().hasHeightForWidth());
        spnSparsify->setSizePolicy(sizePolicy);
        spnSparsify->setMinimumSize(QSize(30, 0));
        spnSparsify->setFrame(false);
        spnSparsify->setAlignment(Qt::AlignRight);
        spnSparsify->setDecimals(1);
        spnSparsify->setMaximum(65536);
        spnSparsify->setValue(10);

        gridLayout1->addWidget(spnSparsify, 5, 1, 1, 1);

        chkPed = new QCheckBox(grpData);
        chkPed->setObjectName(QStringLiteral("chkPed"));
        chkPed->setChecked(true);

        gridLayout1->addWidget(chkPed, 4, 0, 1, 1);

        chkSparsify = new QCheckBox(grpData);
        chkSparsify->setObjectName(QStringLiteral("chkSparsify"));
        chkSparsify->setChecked(true);

        gridLayout1->addWidget(chkSparsify, 5, 0, 1, 1);

        spnPed = new QDoubleSpinBox(grpData);
        spnPed->setObjectName(QStringLiteral("spnPed"));
        sizePolicy.setHeightForWidth(spnPed->sizePolicy().hasHeightForWidth());
        spnPed->setSizePolicy(sizePolicy);
        spnPed->setMinimumSize(QSize(30, 0));
        spnPed->setFrame(false);
        spnPed->setAlignment(Qt::AlignRight);
        spnPed->setDecimals(1);
        spnPed->setMaximum(65536);
        spnPed->setValue(2);

        gridLayout1->addWidget(spnPed, 4, 1, 1, 1);

        chkNoise = new QCheckBox(grpData);
        chkNoise->setObjectName(QStringLiteral("chkNoise"));
        chkNoise->setChecked(true);

        gridLayout1->addWidget(chkNoise, 3, 0, 1, 1);

        chkCMmean = new QCheckBox(grpData);
        chkCMmean->setObjectName(QStringLiteral("chkCMmean"));
        chkCMmean->setChecked(true);

        gridLayout1->addWidget(chkCMmean, 1, 0, 1, 1);

        spnPulse = new QDoubleSpinBox(grpData);
        spnPulse->setObjectName(QStringLiteral("spnPulse"));
        sizePolicy.setHeightForWidth(spnPulse->sizePolicy().hasHeightForWidth());
        spnPulse->setSizePolicy(sizePolicy);
        spnPulse->setMinimumSize(QSize(30, 0));
        spnPulse->setFrame(false);
        spnPulse->setAlignment(Qt::AlignRight);
        spnPulse->setDecimals(1);
        spnPulse->setMaximum(65536);
        spnPulse->setValue(20);

        gridLayout1->addWidget(spnPulse, 0, 1, 1, 1);

        chkCMstdev = new QCheckBox(grpData);
        chkCMstdev->setObjectName(QStringLiteral("chkCMstdev"));
        chkCMstdev->setChecked(true);

        gridLayout1->addWidget(chkCMstdev, 2, 0, 1, 1);

        spnCMmean = new QDoubleSpinBox(grpData);
        spnCMmean->setObjectName(QStringLiteral("spnCMmean"));
        spnCMmean->setFrame(false);
        spnCMmean->setAlignment(Qt::AlignRight);
        spnCMmean->setDecimals(1);
        spnCMmean->setMaximum(65536);
        spnCMmean->setValue(20);

        gridLayout1->addWidget(spnCMmean, 1, 1, 1, 1);

        spnCMstdev = new QDoubleSpinBox(grpData);
        spnCMstdev->setObjectName(QStringLiteral("spnCMstdev"));
        spnCMstdev->setFrame(false);
        spnCMstdev->setAlignment(Qt::AlignRight);
        spnCMstdev->setDecimals(1);
        spnCMstdev->setMaximum(65536);
        spnCMstdev->setValue(5);

        gridLayout1->addWidget(spnCMstdev, 2, 1, 1, 1);

        chkPulses = new QCheckBox(grpData);
        chkPulses->setObjectName(QStringLiteral("chkPulses"));
        chkPulses->setChecked(true);

        gridLayout1->addWidget(chkPulses, 0, 0, 1, 1);


        vboxLayout1->addLayout(gridLayout1);


        vboxLayout->addWidget(grpData);

        grpTriggers = new QGroupBox(centralwidget);
        grpTriggers->setObjectName(QStringLiteral("grpTriggers"));
        hboxLayout2 = new QHBoxLayout(grpTriggers);
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(0);
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        radManual = new QRadioButton(grpTriggers);
        radManual->setObjectName(QStringLiteral("radManual"));
        radManual->setChecked(true);

        vboxLayout2->addWidget(radManual);

        radFixed = new QRadioButton(grpTriggers);
        radFixed->setObjectName(QStringLiteral("radFixed"));

        vboxLayout2->addWidget(radFixed);

        radPoisson = new QRadioButton(grpTriggers);
        radPoisson->setObjectName(QStringLiteral("radPoisson"));

        vboxLayout2->addWidget(radPoisson);


        hboxLayout2->addLayout(vboxLayout2);

        vboxLayout3 = new QVBoxLayout();
        vboxLayout3->setSpacing(0);
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
        vboxLayout3->setObjectName(QStringLiteral("vboxLayout3"));
        gridLayout2 = new QGridLayout();
        gridLayout2->setSpacing(0);
#ifndef Q_OS_MAC
        gridLayout2->setContentsMargins(0, 0, 0, 0);
#endif
        gridLayout2->setObjectName(QStringLiteral("gridLayout2"));
        spnFreq = new QDoubleSpinBox(grpTriggers);
        spnFreq->setObjectName(QStringLiteral("spnFreq"));
        sizePolicy.setHeightForWidth(spnFreq->sizePolicy().hasHeightForWidth());
        spnFreq->setSizePolicy(sizePolicy);
        spnFreq->setMinimumSize(QSize(30, 0));
        spnFreq->setFrame(false);
        spnFreq->setAlignment(Qt::AlignRight);
        spnFreq->setMaximum(100000);
        spnFreq->setValue(1);

        gridLayout2->addWidget(spnFreq, 0, 1, 1, 1);

        spnPeriod = new QDoubleSpinBox(grpTriggers);
        spnPeriod->setObjectName(QStringLiteral("spnPeriod"));
        sizePolicy.setHeightForWidth(spnPeriod->sizePolicy().hasHeightForWidth());
        spnPeriod->setSizePolicy(sizePolicy);
        spnPeriod->setMinimumSize(QSize(30, 0));
        spnPeriod->setFrame(false);
        spnPeriod->setAlignment(Qt::AlignRight);
        spnPeriod->setDecimals(1);
        spnPeriod->setMaximum(1e+09);
        spnPeriod->setValue(1000);

        gridLayout2->addWidget(spnPeriod, 1, 1, 1, 1);

        lblFreq = new QLabel(grpTriggers);
        lblFreq->setObjectName(QStringLiteral("lblFreq"));

        gridLayout2->addWidget(lblFreq, 0, 0, 1, 1);

        lblPeriod = new QLabel(grpTriggers);
        lblPeriod->setObjectName(QStringLiteral("lblPeriod"));

        gridLayout2->addWidget(lblPeriod, 1, 0, 1, 1);


        vboxLayout3->addLayout(gridLayout2);

        btnTrigger = new QPushButton(grpTriggers);
        btnTrigger->setObjectName(QStringLiteral("btnTrigger"));

        vboxLayout3->addWidget(btnTrigger);


        hboxLayout2->addLayout(vboxLayout3);


        vboxLayout->addWidget(grpTriggers);


        hboxLayout->addLayout(vboxLayout);

        vboxLayout4 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout4->setSpacing(6);
#endif
        vboxLayout4->setContentsMargins(0, 0, 0, 0);
        vboxLayout4->setObjectName(QStringLiteral("vboxLayout4"));
        lblPreview = new QLabel(centralwidget);
        lblPreview->setObjectName(QStringLiteral("lblPreview"));

        vboxLayout4->addWidget(lblPreview);

        grphPreview = new QGraphicsView(centralwidget);
        grphPreview->setObjectName(QStringLiteral("grphPreview"));

        vboxLayout4->addWidget(grphPreview);

        grpStatus = new QGroupBox(centralwidget);
        grpStatus->setObjectName(QStringLiteral("grpStatus"));
        vboxLayout5 = new QVBoxLayout(grpStatus);
#ifndef Q_OS_MAC
        vboxLayout5->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout5->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout5->setObjectName(QStringLiteral("vboxLayout5"));
        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(0);
#ifndef Q_OS_MAC
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout3->setObjectName(QStringLiteral("hboxLayout3"));
        txtStatus = new QLineEdit(grpStatus);
        txtStatus->setObjectName(QStringLiteral("txtStatus"));

        hboxLayout3->addWidget(txtStatus);

        cmbStatus = new QComboBox(grpStatus);
        cmbStatus->setObjectName(QStringLiteral("cmbStatus"));

        hboxLayout3->addWidget(cmbStatus);


        vboxLayout5->addLayout(hboxLayout3);

        lblStatus = new QLabel(grpStatus);
        lblStatus->setObjectName(QStringLiteral("lblStatus"));

        vboxLayout5->addWidget(lblStatus);

        line = new QFrame(grpStatus);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        vboxLayout5->addWidget(line);

        gridLayout3 = new QGridLayout();
        gridLayout3->setSpacing(0);
        gridLayout3->setContentsMargins(0, 0, 0, 0);
        gridLayout3->setObjectName(QStringLiteral("gridLayout3"));
        txtDataColl = new QLineEdit(grpStatus);
        txtDataColl->setObjectName(QStringLiteral("txtDataColl"));
        txtDataColl->setReadOnly(true);

        gridLayout3->addWidget(txtDataColl, 4, 1, 1, 1);

        txtState = new QLineEdit(grpStatus);
        txtState->setObjectName(QStringLiteral("txtState"));
        txtState->setReadOnly(true);

        gridLayout3->addWidget(txtState, 2, 1, 1, 1);

        lblRun = new QLabel(grpStatus);
        lblRun->setObjectName(QStringLiteral("lblRun"));

        gridLayout3->addWidget(lblRun, 1, 0, 1, 1);

        lblState = new QLabel(grpStatus);
        lblState->setObjectName(QStringLiteral("lblState"));

        gridLayout3->addWidget(lblState, 2, 0, 1, 1);

        lblConfig = new QLabel(grpStatus);
        lblConfig->setObjectName(QStringLiteral("lblConfig"));

        gridLayout3->addWidget(lblConfig, 3, 0, 1, 1);

        lblDataColl = new QLabel(grpStatus);
        lblDataColl->setObjectName(QStringLiteral("lblDataColl"));

        gridLayout3->addWidget(lblDataColl, 4, 0, 1, 1);

        txtConfig = new QLineEdit(grpStatus);
        txtConfig->setObjectName(QStringLiteral("txtConfig"));
        txtConfig->setReadOnly(true);

        gridLayout3->addWidget(txtConfig, 3, 1, 1, 1);

        lblEvent = new QLabel(grpStatus);
        lblEvent->setObjectName(QStringLiteral("lblEvent"));

        gridLayout3->addWidget(lblEvent, 0, 0, 1, 1);

        txtRun = new QLineEdit(grpStatus);
        txtRun->setObjectName(QStringLiteral("txtRun"));
        txtRun->setReadOnly(true);

        gridLayout3->addWidget(txtRun, 1, 1, 1, 1);

        txtEvent = new QLineEdit(grpStatus);
        txtEvent->setObjectName(QStringLiteral("txtEvent"));
        txtEvent->setReadOnly(true);

        gridLayout3->addWidget(txtEvent, 0, 1, 1, 1);


        vboxLayout5->addLayout(gridLayout3);


        vboxLayout4->addWidget(grpStatus);

        btnQuit = new QPushButton(centralwidget);
        btnQuit->setObjectName(QStringLiteral("btnQuit"));

        vboxLayout4->addWidget(btnQuit);


        hboxLayout->addLayout(vboxLayout4);

        wndProd->setCentralWidget(centralwidget);
        menubar = new QMenuBar(wndProd);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 705, 22));
        wndProd->setMenuBar(menubar);
#ifndef QT_NO_SHORTCUT
        lblWidth->setBuddy(spnWidth);
        lblProfile->setBuddy(comboBox);
        lblX->setBuddy(spnX);
        lblHeight->setBuddy(spnHeight);
        lblRadius->setBuddy(spnRadius);
        lblY->setBuddy(spnY);
        lblFreq->setBuddy(spnFreq);
        lblPeriod->setBuddy(spnPeriod);
        lblPreview->setBuddy(grphPreview);
        lblStatus->setBuddy(txtStatus);
        lblRun->setBuddy(txtRun);
        lblState->setBuddy(txtState);
        lblConfig->setBuddy(txtConfig);
        lblDataColl->setBuddy(txtDataColl);
        lblEvent->setBuddy(txtEvent);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(radFile, txtFile);
        QWidget::setTabOrder(txtFile, radCustom);
        QWidget::setTabOrder(radCustom, spnWidth);
        QWidget::setTabOrder(spnWidth, spnHeight);
        QWidget::setTabOrder(spnHeight, comboBox);
        QWidget::setTabOrder(comboBox, spnRadius);
        QWidget::setTabOrder(spnRadius, spnX);
        QWidget::setTabOrder(spnX, spnY);
        QWidget::setTabOrder(spnY, chkPulses);
        QWidget::setTabOrder(chkPulses, spnPulse);
        QWidget::setTabOrder(spnPulse, chkCMmean);
        QWidget::setTabOrder(chkCMmean, spnCMmean);
        QWidget::setTabOrder(spnCMmean, chkCMstdev);
        QWidget::setTabOrder(chkCMstdev, spnCMstdev);
        QWidget::setTabOrder(spnCMstdev, chkNoise);
        QWidget::setTabOrder(chkNoise, spnNoise);
        QWidget::setTabOrder(spnNoise, chkPed);
        QWidget::setTabOrder(chkPed, spnPed);
        QWidget::setTabOrder(spnPed, chkSparsify);
        QWidget::setTabOrder(chkSparsify, spnSparsify);
        QWidget::setTabOrder(spnSparsify, radManual);
        QWidget::setTabOrder(radManual, radFixed);
        QWidget::setTabOrder(radFixed, radPoisson);
        QWidget::setTabOrder(radPoisson, spnFreq);
        QWidget::setTabOrder(spnFreq, spnPeriod);
        QWidget::setTabOrder(spnPeriod, btnTrigger);
        QWidget::setTabOrder(btnTrigger, grphPreview);
        QWidget::setTabOrder(grphPreview, txtStatus);
        QWidget::setTabOrder(txtStatus, cmbStatus);
        QWidget::setTabOrder(cmbStatus, txtEvent);
        QWidget::setTabOrder(txtEvent, txtRun);
        QWidget::setTabOrder(txtRun, txtState);
        QWidget::setTabOrder(txtState, txtConfig);
        QWidget::setTabOrder(txtConfig, txtDataColl);
        QWidget::setTabOrder(txtDataColl, btnQuit);

        retranslateUi(wndProd);
        QObject::connect(btnQuit, SIGNAL(clicked()), wndProd, SLOT(close()));

        QMetaObject::connectSlotsByName(wndProd);
    } // setupUi

    void retranslateUi(QMainWindow *wndProd)
    {
        wndProd->setWindowTitle(QApplication::translate("wndProd", "eudaq Dummy Producer", 0));
        grpData->setTitle(QApplication::translate("wndProd", "Data", 0));
        radFile->setText(QApplication::translate("wndProd", "File:", 0));
        radCustom->setText(QApplication::translate("wndProd", "Custom:", 0));
        lblWidth->setText(QApplication::translate("wndProd", "Width:", 0));
        lblProfile->setText(QApplication::translate("wndProd", "Profile:", 0));
        lblX->setText(QApplication::translate("wndProd", "X:", 0));
        lblHeight->setText(QApplication::translate("wndProd", "Height:", 0));
        lblRadius->setText(QApplication::translate("wndProd", "Radius:", 0));
        lblY->setText(QApplication::translate("wndProd", "Y:", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("wndProd", "Circle", 0)
         << QApplication::translate("wndProd", "Gaussian", 0)
        );
        spnNoise->setSuffix(QApplication::translate("wndProd", " ADC", 0));
        spnSparsify->setSuffix(QApplication::translate("wndProd", " ADC", 0));
        chkPed->setText(QApplication::translate("wndProd", "Pedestal:", 0));
        chkSparsify->setText(QApplication::translate("wndProd", "Sparsify:", 0));
        spnPed->setSuffix(QApplication::translate("wndProd", " ADC", 0));
        chkNoise->setText(QApplication::translate("wndProd", "Noise:", 0));
        chkCMmean->setText(QApplication::translate("wndProd", "CM mean:", 0));
        spnPulse->setSuffix(QApplication::translate("wndProd", " ADC", 0));
        chkCMstdev->setText(QApplication::translate("wndProd", "CM st.dev:", 0));
        spnCMmean->setSuffix(QApplication::translate("wndProd", " ADC", 0));
        spnCMstdev->setSuffix(QApplication::translate("wndProd", " ADC", 0));
        chkPulses->setText(QApplication::translate("wndProd", "Pulses:", 0));
        grpTriggers->setTitle(QApplication::translate("wndProd", "Triggers", 0));
        radManual->setText(QApplication::translate("wndProd", "Manual", 0));
        radFixed->setText(QApplication::translate("wndProd", "Fixed", 0));
        radPoisson->setText(QApplication::translate("wndProd", "Poisson", 0));
        spnFreq->setSuffix(QApplication::translate("wndProd", " Hz", 0));
        spnPeriod->setSuffix(QApplication::translate("wndProd", " ms", 0));
        lblFreq->setText(QApplication::translate("wndProd", "Freq:", 0));
        lblPeriod->setText(QApplication::translate("wndProd", "Period:", 0));
        btnTrigger->setText(QApplication::translate("wndProd", "Trigger!", 0));
        lblPreview->setText(QApplication::translate("wndProd", "Preview:", 0));
        grpStatus->setTitle(QApplication::translate("wndProd", "Status", 0));
        cmbStatus->clear();
        cmbStatus->insertItems(0, QStringList()
         << QApplication::translate("wndProd", "Select:", 0)
         << QApplication::translate("wndProd", "OK", 0)
         << QApplication::translate("wndProd", "Warn", 0)
         << QApplication::translate("wndProd", "Error", 0)
        );
        lblStatus->setText(QApplication::translate("wndProd", "OK", 0));
        lblRun->setText(QApplication::translate("wndProd", "Run Num:", 0));
        lblState->setText(QApplication::translate("wndProd", "State:", 0));
        lblConfig->setText(QApplication::translate("wndProd", "Config:", 0));
        lblDataColl->setText(QApplication::translate("wndProd", "DataCollector:", 0));
        lblEvent->setText(QApplication::translate("wndProd", "Event Num:", 0));
        btnQuit->setText(QApplication::translate("wndProd", "Quit", 0));
    } // retranslateUi

};

namespace Ui {
    class wndProd: public Ui_wndProd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EUPROD_H
