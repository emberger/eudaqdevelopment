/****************************************************************************
** Meta object code from reading C++ file 'euLog.hh'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "euLog.hh"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'euLog.hh' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_LogCollectorGUI_t {
    QByteArrayData data[13];
    char stringdata0[187];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LogCollectorGUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LogCollectorGUI_t qt_meta_stringdata_LogCollectorGUI = {
    {
QT_MOC_LITERAL(0, 0, 15), // "LogCollectorGUI"
QT_MOC_LITERAL(1, 16, 10), // "RecMessage"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 17), // "eudaq::LogMessage"
QT_MOC_LITERAL(4, 46, 3), // "msg"
QT_MOC_LITERAL(5, 50, 31), // "on_cmbLevel_currentIndexChanged"
QT_MOC_LITERAL(6, 82, 5), // "index"
QT_MOC_LITERAL(7, 88, 30), // "on_cmbFrom_currentIndexChanged"
QT_MOC_LITERAL(8, 119, 4), // "text"
QT_MOC_LITERAL(9, 124, 28), // "on_txtSearch_editingFinished"
QT_MOC_LITERAL(10, 153, 20), // "on_viewLog_activated"
QT_MOC_LITERAL(11, 174, 1), // "i"
QT_MOC_LITERAL(12, 176, 10) // "AddMessage"

    },
    "LogCollectorGUI\0RecMessage\0\0"
    "eudaq::LogMessage\0msg\0"
    "on_cmbLevel_currentIndexChanged\0index\0"
    "on_cmbFrom_currentIndexChanged\0text\0"
    "on_txtSearch_editingFinished\0"
    "on_viewLog_activated\0i\0AddMessage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LogCollectorGUI[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   47,    2, 0x08 /* Private */,
       7,    1,   50,    2, 0x08 /* Private */,
       9,    0,   53,    2, 0x08 /* Private */,
      10,    1,   54,    2, 0x08 /* Private */,
      12,    1,   57,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   11,
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void LogCollectorGUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LogCollectorGUI *_t = static_cast<LogCollectorGUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->RecMessage((*reinterpret_cast< const eudaq::LogMessage(*)>(_a[1]))); break;
        case 1: _t->on_cmbLevel_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_cmbFrom_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_txtSearch_editingFinished(); break;
        case 4: _t->on_viewLog_activated((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->AddMessage((*reinterpret_cast< const eudaq::LogMessage(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (LogCollectorGUI::*_t)(const eudaq::LogMessage & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LogCollectorGUI::RecMessage)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject LogCollectorGUI::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_LogCollectorGUI.data,
      qt_meta_data_LogCollectorGUI,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *LogCollectorGUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LogCollectorGUI::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_LogCollectorGUI.stringdata0))
        return static_cast<void*>(const_cast< LogCollectorGUI*>(this));
    if (!strcmp(_clname, "Ui::wndLog"))
        return static_cast< Ui::wndLog*>(const_cast< LogCollectorGUI*>(this));
    if (!strcmp(_clname, "eudaq::LogCollector"))
        return static_cast< eudaq::LogCollector*>(const_cast< LogCollectorGUI*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int LogCollectorGUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void LogCollectorGUI::RecMessage(const eudaq::LogMessage & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
