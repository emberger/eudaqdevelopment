/****************************************************************************
** Meta object code from reading C++ file 'euProd.hh'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "euProd.hh"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'euProd.hh' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ProducerGUI_t {
    QByteArrayData data[5];
    char stringdata0[55];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProducerGUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProducerGUI_t qt_meta_stringdata_ProducerGUI = {
    {
QT_MOC_LITERAL(0, 0, 11), // "ProducerGUI"
QT_MOC_LITERAL(1, 12, 6), // "Author"
QT_MOC_LITERAL(2, 19, 12), // "Emlyn Corrin"
QT_MOC_LITERAL(3, 32, 21), // "on_btnTrigger_clicked"
QT_MOC_LITERAL(4, 54, 0) // ""

    },
    "ProducerGUI\0Author\0Emlyn Corrin\0"
    "on_btnTrigger_clicked\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProducerGUI[] = {

 // content:
       7,       // revision
       0,       // classname
       1,   14, // classinfo
       1,   16, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,

 // slots: name, argc, parameters, tag, flags
       3,    0,   21,    4, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void ProducerGUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProducerGUI *_t = static_cast<ProducerGUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btnTrigger_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject ProducerGUI::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ProducerGUI.data,
      qt_meta_data_ProducerGUI,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ProducerGUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProducerGUI::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ProducerGUI.stringdata0))
        return static_cast<void*>(const_cast< ProducerGUI*>(this));
    if (!strcmp(_clname, "Ui::wndProd"))
        return static_cast< Ui::wndProd*>(const_cast< ProducerGUI*>(this));
    if (!strcmp(_clname, "eudaq::Producer"))
        return static_cast< eudaq::Producer*>(const_cast< ProducerGUI*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ProducerGUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
