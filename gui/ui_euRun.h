/********************************************************************************
** Form generated from reading UI file 'euRun.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EURUN_H
#define UI_EURUN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_wndRun
{
public:
    QWidget *centralwidget;
    QVBoxLayout *vboxLayout;
    QGroupBox *grpCurrentState;
    QHBoxLayout *hboxLayout;
    QLabel *lblCurrent;
    QGroupBox *grpControl;
    QGridLayout *gridLayout;
    QLabel *lblInit;
    QLineEdit *txtInitFileName;
    QPushButton *btnLoadInit;
    QPushButton *btnInit;
    QLabel *lblConfig;
    QLineEdit *txtConfigFileName;
    QPushButton *btnLoadConf;
    QPushButton *btnConfig;
    QLabel *lblNextRunNumber;
    QLineEdit *txtNextRunNumber;
    QPushButton *btnStart;
    QPushButton *btnStop;
    QPushButton *btnReset;
    QPushButton *btnTerminate;
    QLabel *lblLogmsg;
    QLineEdit *txtLogmsg;
    QPushButton *btnLog;
    QGroupBox *grpStatus;
    QGridLayout *gridLayout1;
    QGroupBox *grpConnections;
    QVBoxLayout *vboxLayout1;
    QTreeView *viewConn;
    QMenuBar *menubar;

    void setupUi(QMainWindow *wndRun)
    {
        if (wndRun->objectName().isEmpty())
            wndRun->setObjectName(QStringLiteral("wndRun"));
        wndRun->setEnabled(true);
        wndRun->resize(350, 373);
        centralwidget = new QWidget(wndRun);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        vboxLayout = new QVBoxLayout(centralwidget);
        vboxLayout->setSpacing(4);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        vboxLayout->setContentsMargins(4, 4, 4, 4);
        grpCurrentState = new QGroupBox(centralwidget);
        grpCurrentState->setObjectName(QStringLiteral("grpCurrentState"));
        hboxLayout = new QHBoxLayout(grpCurrentState);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        lblCurrent = new QLabel(grpCurrentState);
        lblCurrent->setObjectName(QStringLiteral("lblCurrent"));

        hboxLayout->addWidget(lblCurrent);


        vboxLayout->addWidget(grpCurrentState);

        grpControl = new QGroupBox(centralwidget);
        grpControl->setObjectName(QStringLiteral("grpControl"));
        gridLayout = new QGridLayout(grpControl);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(8);
        gridLayout->setVerticalSpacing(4);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lblInit = new QLabel(grpControl);
        lblInit->setObjectName(QStringLiteral("lblInit"));

        gridLayout->addWidget(lblInit, 0, 0, 1, 1);

        txtInitFileName = new QLineEdit(grpControl);
        txtInitFileName->setObjectName(QStringLiteral("txtInitFileName"));

        gridLayout->addWidget(txtInitFileName, 0, 2, 1, 1);

        btnLoadInit = new QPushButton(grpControl);
        btnLoadInit->setObjectName(QStringLiteral("btnLoadInit"));

        gridLayout->addWidget(btnLoadInit, 0, 3, 1, 1);

        btnInit = new QPushButton(grpControl);
        btnInit->setObjectName(QStringLiteral("btnInit"));

        gridLayout->addWidget(btnInit, 0, 4, 1, 1);

        lblConfig = new QLabel(grpControl);
        lblConfig->setObjectName(QStringLiteral("lblConfig"));

        gridLayout->addWidget(lblConfig, 1, 0, 1, 1);

        txtConfigFileName = new QLineEdit(grpControl);
        txtConfigFileName->setObjectName(QStringLiteral("txtConfigFileName"));

        gridLayout->addWidget(txtConfigFileName, 1, 2, 1, 1);

        btnLoadConf = new QPushButton(grpControl);
        btnLoadConf->setObjectName(QStringLiteral("btnLoadConf"));

        gridLayout->addWidget(btnLoadConf, 1, 3, 1, 1);

        btnConfig = new QPushButton(grpControl);
        btnConfig->setObjectName(QStringLiteral("btnConfig"));
        btnConfig->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnConfig->sizePolicy().hasHeightForWidth());
        btnConfig->setSizePolicy(sizePolicy);

        gridLayout->addWidget(btnConfig, 1, 4, 1, 1);

        lblNextRunNumber = new QLabel(grpControl);
        lblNextRunNumber->setObjectName(QStringLiteral("lblNextRunNumber"));

        gridLayout->addWidget(lblNextRunNumber, 2, 0, 1, 1);

        txtNextRunNumber = new QLineEdit(grpControl);
        txtNextRunNumber->setObjectName(QStringLiteral("txtNextRunNumber"));

        gridLayout->addWidget(txtNextRunNumber, 2, 2, 1, 1);

        btnStart = new QPushButton(grpControl);
        btnStart->setObjectName(QStringLiteral("btnStart"));
        btnStart->setEnabled(false);

        gridLayout->addWidget(btnStart, 2, 3, 1, 1);

        btnStop = new QPushButton(grpControl);
        btnStop->setObjectName(QStringLiteral("btnStop"));
        btnStop->setEnabled(false);

        gridLayout->addWidget(btnStop, 2, 4, 1, 1);

        btnReset = new QPushButton(grpControl);
        btnReset->setObjectName(QStringLiteral("btnReset"));
        btnReset->setEnabled(false);

        gridLayout->addWidget(btnReset, 3, 3, 1, 1);

        btnTerminate = new QPushButton(grpControl);
        btnTerminate->setObjectName(QStringLiteral("btnTerminate"));
        btnTerminate->setEnabled(false);

        gridLayout->addWidget(btnTerminate, 3, 4, 1, 1);

        lblLogmsg = new QLabel(grpControl);
        lblLogmsg->setObjectName(QStringLiteral("lblLogmsg"));

        gridLayout->addWidget(lblLogmsg, 4, 0, 1, 1);

        txtLogmsg = new QLineEdit(grpControl);
        txtLogmsg->setObjectName(QStringLiteral("txtLogmsg"));

        gridLayout->addWidget(txtLogmsg, 4, 2, 1, 1);

        btnLog = new QPushButton(grpControl);
        btnLog->setObjectName(QStringLiteral("btnLog"));
        btnLog->setEnabled(false);

        gridLayout->addWidget(btnLog, 4, 3, 1, 1);


        vboxLayout->addWidget(grpControl);

        grpStatus = new QGroupBox(centralwidget);
        grpStatus->setObjectName(QStringLiteral("grpStatus"));
        gridLayout1 = new QGridLayout(grpStatus);
        gridLayout1->setObjectName(QStringLiteral("gridLayout1"));
        gridLayout1->setHorizontalSpacing(8);
        gridLayout1->setVerticalSpacing(4);
        gridLayout1->setContentsMargins(0, 0, 0, 0);

        vboxLayout->addWidget(grpStatus);

        grpConnections = new QGroupBox(centralwidget);
        grpConnections->setObjectName(QStringLiteral("grpConnections"));
        vboxLayout1 = new QVBoxLayout(grpConnections);
        vboxLayout1->setSpacing(4);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        viewConn = new QTreeView(grpConnections);
        viewConn->setObjectName(QStringLiteral("viewConn"));
        viewConn->setRootIsDecorated(false);
        viewConn->setSortingEnabled(true);

        vboxLayout1->addWidget(viewConn);


        vboxLayout->addWidget(grpConnections);

        wndRun->setCentralWidget(centralwidget);
        menubar = new QMenuBar(wndRun);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 350, 25));
        wndRun->setMenuBar(menubar);

        retranslateUi(wndRun);
        QObject::connect(txtLogmsg, SIGNAL(returnPressed()), btnLog, SLOT(animateClick()));

        QMetaObject::connectSlotsByName(wndRun);
    } // setupUi

    void retranslateUi(QMainWindow *wndRun)
    {
        wndRun->setWindowTitle(QApplication::translate("wndRun", "eudaq Run Control", 0));
        grpCurrentState->setTitle(QApplication::translate("wndRun", "State:", 0));
        lblCurrent->setText(QApplication::translate("wndRun", "Current State :", 0));
        grpControl->setTitle(QApplication::translate("wndRun", "Control", 0));
        lblInit->setText(QApplication::translate("wndRun", "Init file:", 0));
        txtInitFileName->setText(QApplication::translate("wndRun", "initialisation file not set", 0));
        btnLoadInit->setText(QApplication::translate("wndRun", "Load", 0));
        btnInit->setText(QApplication::translate("wndRun", "Init", 0));
        lblConfig->setText(QApplication::translate("wndRun", "Config file: ", 0));
#ifndef QT_NO_WHATSTHIS
        txtConfigFileName->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        txtConfigFileName->setText(QApplication::translate("wndRun", "configuration file not set", 0));
        btnLoadConf->setText(QApplication::translate("wndRun", "Load", 0));
#ifndef QT_NO_TOOLTIP
        btnConfig->setToolTip(QApplication::translate("wndRun", "Configure using the selected configuration", 0));
#endif // QT_NO_TOOLTIP
        btnConfig->setText(QApplication::translate("wndRun", "Config", 0));
        lblNextRunNumber->setText(QApplication::translate("wndRun", "Next RunN: ", 0));
#ifndef QT_NO_TOOLTIP
        btnStart->setToolTip(QApplication::translate("wndRun", "Start a run", 0));
#endif // QT_NO_TOOLTIP
        btnStart->setText(QApplication::translate("wndRun", "Start", 0));
#ifndef QT_NO_TOOLTIP
        btnStop->setToolTip(QApplication::translate("wndRun", "Stop a run", 0));
#endif // QT_NO_TOOLTIP
        btnStop->setText(QApplication::translate("wndRun", "Stop", 0));
#ifndef QT_NO_TOOLTIP
        btnReset->setToolTip(QApplication::translate("wndRun", "Reset all the DAQ clients", 0));
#endif // QT_NO_TOOLTIP
        btnReset->setText(QApplication::translate("wndRun", "Reset", 0));
#ifndef QT_NO_TOOLTIP
        btnTerminate->setToolTip(QApplication::translate("wndRun", "Terminate the DAQ session", 0));
#endif // QT_NO_TOOLTIP
        btnTerminate->setText(QApplication::translate("wndRun", "Terminate", 0));
        lblLogmsg->setText(QApplication::translate("wndRun", "Log: ", 0));
#ifndef QT_NO_TOOLTIP
        txtLogmsg->setToolTip(QApplication::translate("wndRun", "Send a message to the LogCollector", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        btnLog->setToolTip(QApplication::translate("wndRun", "Send a log message", 0));
#endif // QT_NO_TOOLTIP
        btnLog->setText(QApplication::translate("wndRun", "Log", 0));
        grpConnections->setTitle(QApplication::translate("wndRun", "Connections", 0));
#ifndef QT_NO_TOOLTIP
        viewConn->setToolTip(QApplication::translate("wndRun", "Processes connected to the Run Control", 0));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

namespace Ui {
    class wndRun: public Ui_wndRun {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EURUN_H
