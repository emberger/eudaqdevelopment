// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME StdEventMonitor_ROOT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/home/calice/eudaq/monitors/onlinemon/include/OnlineMonWindow.hh"
#include "/home/calice/eudaq/monitors/onlinemon/include/CheckEOF.hh"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *BaseCollection_Dictionary();
   static void BaseCollection_TClassManip(TClass*);
   static void delete_BaseCollection(void *p);
   static void deleteArray_BaseCollection(void *p);
   static void destruct_BaseCollection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BaseCollection*)
   {
      ::BaseCollection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::BaseCollection));
      static ::ROOT::TGenericClassInfo 
         instance("BaseCollection", "BaseCollection.hh", 44,
                  typeid(::BaseCollection), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &BaseCollection_Dictionary, isa_proxy, 1,
                  sizeof(::BaseCollection) );
      instance.SetDelete(&delete_BaseCollection);
      instance.SetDeleteArray(&deleteArray_BaseCollection);
      instance.SetDestructor(&destruct_BaseCollection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BaseCollection*)
   {
      return GenerateInitInstanceLocal((::BaseCollection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::BaseCollection*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *BaseCollection_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::BaseCollection*)0x0)->GetClass();
      BaseCollection_TClassManip(theClass);
   return theClass;
   }

   static void BaseCollection_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CheckEOF_Dictionary();
   static void CheckEOF_TClassManip(TClass*);
   static void *new_CheckEOF(void *p = 0);
   static void *newArray_CheckEOF(Long_t size, void *p);
   static void delete_CheckEOF(void *p);
   static void deleteArray_CheckEOF(void *p);
   static void destruct_CheckEOF(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CheckEOF*)
   {
      ::CheckEOF *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CheckEOF));
      static ::ROOT::TGenericClassInfo 
         instance("CheckEOF", "CheckEOF.hh", 14,
                  typeid(::CheckEOF), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CheckEOF_Dictionary, isa_proxy, 1,
                  sizeof(::CheckEOF) );
      instance.SetNew(&new_CheckEOF);
      instance.SetNewArray(&newArray_CheckEOF);
      instance.SetDelete(&delete_CheckEOF);
      instance.SetDeleteArray(&deleteArray_CheckEOF);
      instance.SetDestructor(&destruct_CheckEOF);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CheckEOF*)
   {
      return GenerateInitInstanceLocal((::CheckEOF*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::CheckEOF*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CheckEOF_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CheckEOF*)0x0)->GetClass();
      CheckEOF_TClassManip(theClass);
   return theClass;
   }

   static void CheckEOF_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void delete_OnlineMonWindow(void *p);
   static void deleteArray_OnlineMonWindow(void *p);
   static void destruct_OnlineMonWindow(void *p);
   static void streamer_OnlineMonWindow(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::OnlineMonWindow*)
   {
      ::OnlineMonWindow *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::OnlineMonWindow >(0);
      static ::ROOT::TGenericClassInfo 
         instance("OnlineMonWindow", ::OnlineMonWindow::Class_Version(), "OnlineMonWindow.hh", 46,
                  typeid(::OnlineMonWindow), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::OnlineMonWindow::Dictionary, isa_proxy, 16,
                  sizeof(::OnlineMonWindow) );
      instance.SetDelete(&delete_OnlineMonWindow);
      instance.SetDeleteArray(&deleteArray_OnlineMonWindow);
      instance.SetDestructor(&destruct_OnlineMonWindow);
      instance.SetStreamerFunc(&streamer_OnlineMonWindow);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::OnlineMonWindow*)
   {
      return GenerateInitInstanceLocal((::OnlineMonWindow*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr OnlineMonWindow::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *OnlineMonWindow::Class_Name()
{
   return "OnlineMonWindow";
}

//______________________________________________________________________________
const char *OnlineMonWindow::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int OnlineMonWindow::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *OnlineMonWindow::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *OnlineMonWindow::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetClass(); }
   return fgIsA;
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_BaseCollection(void *p) {
      delete ((::BaseCollection*)p);
   }
   static void deleteArray_BaseCollection(void *p) {
      delete [] ((::BaseCollection*)p);
   }
   static void destruct_BaseCollection(void *p) {
      typedef ::BaseCollection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::BaseCollection

namespace ROOT {
   // Wrappers around operator new
   static void *new_CheckEOF(void *p) {
      return  p ? new(p) ::CheckEOF : new ::CheckEOF;
   }
   static void *newArray_CheckEOF(Long_t nElements, void *p) {
      return p ? new(p) ::CheckEOF[nElements] : new ::CheckEOF[nElements];
   }
   // Wrapper around operator delete
   static void delete_CheckEOF(void *p) {
      delete ((::CheckEOF*)p);
   }
   static void deleteArray_CheckEOF(void *p) {
      delete [] ((::CheckEOF*)p);
   }
   static void destruct_CheckEOF(void *p) {
      typedef ::CheckEOF current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CheckEOF

//______________________________________________________________________________
void OnlineMonWindow::Streamer(TBuffer &R__b)
{
   // Stream an object of class OnlineMonWindow.

   TGMainFrame::Streamer(R__b);
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_OnlineMonWindow(void *p) {
      delete ((::OnlineMonWindow*)p);
   }
   static void deleteArray_OnlineMonWindow(void *p) {
      delete [] ((::OnlineMonWindow*)p);
   }
   static void destruct_OnlineMonWindow(void *p) {
      typedef ::OnlineMonWindow current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_OnlineMonWindow(TBuffer &buf, void *obj) {
      ((::OnlineMonWindow*)obj)->::OnlineMonWindow::Streamer(buf);
   }
} // end of namespace ROOT for class ::OnlineMonWindow

namespace {
  void TriggerDictionaryInitialization_libeudaq_StdEventMonitor_ROOT_Impl() {
    static const char* headers[] = {
"include/OnlineMonWindow.hh",
"include/CheckEOF.hh",
0
    };
    static const char* includePaths[] = {
"/home/calice/eudaq/main/lib/core/include",
"/home/calice/eudaq/extern/include",
"/home/calice/eudaq/include",
"/home/calice/root_6.12.06/include",
"/home/calice/eudaq/monitors/onlinemon/.",
"/home/calice/eudaq/monitors/onlinemon/include",
"/home/calice/eudaq",
"/home/calice/root_6.12.06/include",
"/home/calice/eudaq/monitors/onlinemon/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libeudaq_StdEventMonitor_ROOT dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$BaseCollection.hh")))  __attribute__((annotate("$clingAutoload$include/OnlineMonWindow.hh")))  BaseCollection;
class __attribute__((annotate("$clingAutoload$CheckEOF.hh")))  __attribute__((annotate("$clingAutoload$include/OnlineMonWindow.hh")))  CheckEOF;
class __attribute__((annotate("$clingAutoload$include/OnlineMonWindow.hh")))  OnlineMonWindow;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libeudaq_StdEventMonitor_ROOT dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef EUDAQ_LIB_ROOT6
  #define EUDAQ_LIB_ROOT6 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "include/OnlineMonWindow.hh"
#include "include/CheckEOF.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"BaseCollection", payloadCode, "@",
"CheckEOF", payloadCode, "@",
"OnlineMonWindow", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libeudaq_StdEventMonitor_ROOT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libeudaq_StdEventMonitor_ROOT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libeudaq_StdEventMonitor_ROOT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libeudaq_StdEventMonitor_ROOT() {
  TriggerDictionaryInitialization_libeudaq_StdEventMonitor_ROOT_Impl();
}
