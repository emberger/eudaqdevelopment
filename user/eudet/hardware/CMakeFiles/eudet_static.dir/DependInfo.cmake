# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/calice/eudaq/user/eudet/hardware/src/FmctluController.cc" "/home/calice/eudaq/user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o"
  "/home/calice/eudaq/user/eudet/hardware/src/FmctluHardware.cc" "/home/calice/eudaq/user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o"
  "/home/calice/eudaq/user/eudet/hardware/src/FmctluI2c.cc" "/home/calice/eudaq/user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o"
  "/home/calice/eudaq/user/eudet/hardware/src/MinitluController.cc" "/home/calice/eudaq/user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o"
  "/home/calice/eudaq/user/eudet/hardware/src/NiController.cc" "/home/calice/eudaq/user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EUDAQ_FUNC=__PRETTY_FUNCTION__ "
  "EUDAQ_PLATFORM=PF_LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "main/lib/core/include"
  "extern/include"
  "include"
  "main/lib/core/include/eudaq"
  "main/lib/lcio/include"
  "user/eudet/hardware/include"
  "/opt/cactus/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/calice/eudaq/main/lib/core/CMakeFiles/core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
