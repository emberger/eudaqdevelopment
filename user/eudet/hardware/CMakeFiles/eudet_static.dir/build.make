# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/cmake-3.10.2-Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /opt/cmake-3.10.2-Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/calice/eudaq

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/calice/eudaq

# Include any dependencies generated for this target.
include user/eudet/hardware/CMakeFiles/eudet_static.dir/depend.make

# Include the progress variables for this target.
include user/eudet/hardware/CMakeFiles/eudet_static.dir/progress.make

# Include the compile flags for this target's objects.
include user/eudet/hardware/CMakeFiles/eudet_static.dir/flags.make

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o: user/eudet/hardware/CMakeFiles/eudet_static.dir/flags.make
user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o: user/eudet/hardware/src/MinitluController.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calice/eudaq/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/eudet_static.dir/src/MinitluController.cc.o -c /home/calice/eudaq/user/eudet/hardware/src/MinitluController.cc

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/eudet_static.dir/src/MinitluController.cc.i"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/calice/eudaq/user/eudet/hardware/src/MinitluController.cc > CMakeFiles/eudet_static.dir/src/MinitluController.cc.i

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/eudet_static.dir/src/MinitluController.cc.s"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/calice/eudaq/user/eudet/hardware/src/MinitluController.cc -o CMakeFiles/eudet_static.dir/src/MinitluController.cc.s

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.requires:

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.requires

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.provides: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.requires
	$(MAKE) -f user/eudet/hardware/CMakeFiles/eudet_static.dir/build.make user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.provides.build
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.provides

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.provides.build: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o


user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o: user/eudet/hardware/CMakeFiles/eudet_static.dir/flags.make
user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o: user/eudet/hardware/src/FmctluController.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calice/eudaq/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/eudet_static.dir/src/FmctluController.cc.o -c /home/calice/eudaq/user/eudet/hardware/src/FmctluController.cc

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/eudet_static.dir/src/FmctluController.cc.i"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/calice/eudaq/user/eudet/hardware/src/FmctluController.cc > CMakeFiles/eudet_static.dir/src/FmctluController.cc.i

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/eudet_static.dir/src/FmctluController.cc.s"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/calice/eudaq/user/eudet/hardware/src/FmctluController.cc -o CMakeFiles/eudet_static.dir/src/FmctluController.cc.s

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.requires:

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.requires

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.provides: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.requires
	$(MAKE) -f user/eudet/hardware/CMakeFiles/eudet_static.dir/build.make user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.provides.build
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.provides

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.provides.build: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o


user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o: user/eudet/hardware/CMakeFiles/eudet_static.dir/flags.make
user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o: user/eudet/hardware/src/FmctluHardware.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calice/eudaq/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CXX object user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o -c /home/calice/eudaq/user/eudet/hardware/src/FmctluHardware.cc

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.i"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/calice/eudaq/user/eudet/hardware/src/FmctluHardware.cc > CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.i

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.s"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/calice/eudaq/user/eudet/hardware/src/FmctluHardware.cc -o CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.s

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.requires:

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.requires

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.provides: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.requires
	$(MAKE) -f user/eudet/hardware/CMakeFiles/eudet_static.dir/build.make user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.provides.build
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.provides

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.provides.build: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o


user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o: user/eudet/hardware/CMakeFiles/eudet_static.dir/flags.make
user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o: user/eudet/hardware/src/FmctluI2c.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calice/eudaq/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building CXX object user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o -c /home/calice/eudaq/user/eudet/hardware/src/FmctluI2c.cc

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.i"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/calice/eudaq/user/eudet/hardware/src/FmctluI2c.cc > CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.i

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.s"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/calice/eudaq/user/eudet/hardware/src/FmctluI2c.cc -o CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.s

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.requires:

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.requires

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.provides: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.requires
	$(MAKE) -f user/eudet/hardware/CMakeFiles/eudet_static.dir/build.make user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.provides.build
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.provides

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.provides.build: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o


user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o: user/eudet/hardware/CMakeFiles/eudet_static.dir/flags.make
user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o: user/eudet/hardware/src/NiController.cc
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calice/eudaq/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building CXX object user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/eudet_static.dir/src/NiController.cc.o -c /home/calice/eudaq/user/eudet/hardware/src/NiController.cc

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/eudet_static.dir/src/NiController.cc.i"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/calice/eudaq/user/eudet/hardware/src/NiController.cc > CMakeFiles/eudet_static.dir/src/NiController.cc.i

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/eudet_static.dir/src/NiController.cc.s"
	cd /home/calice/eudaq/user/eudet/hardware && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/calice/eudaq/user/eudet/hardware/src/NiController.cc -o CMakeFiles/eudet_static.dir/src/NiController.cc.s

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.requires:

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.requires

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.provides: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.requires
	$(MAKE) -f user/eudet/hardware/CMakeFiles/eudet_static.dir/build.make user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.provides.build
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.provides

user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.provides.build: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o


# Object files for target eudet_static
eudet_static_OBJECTS = \
"CMakeFiles/eudet_static.dir/src/MinitluController.cc.o" \
"CMakeFiles/eudet_static.dir/src/FmctluController.cc.o" \
"CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o" \
"CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o" \
"CMakeFiles/eudet_static.dir/src/NiController.cc.o"

# External object files for target eudet_static
eudet_static_EXTERNAL_OBJECTS =

user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o
user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o
user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o
user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o
user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o
user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/build.make
user/eudet/hardware/libeudet_static.a: user/eudet/hardware/CMakeFiles/eudet_static.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/calice/eudaq/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Linking CXX static library libeudet_static.a"
	cd /home/calice/eudaq/user/eudet/hardware && $(CMAKE_COMMAND) -P CMakeFiles/eudet_static.dir/cmake_clean_target.cmake
	cd /home/calice/eudaq/user/eudet/hardware && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/eudet_static.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
user/eudet/hardware/CMakeFiles/eudet_static.dir/build: user/eudet/hardware/libeudet_static.a

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/build

user/eudet/hardware/CMakeFiles/eudet_static.dir/requires: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/MinitluController.cc.o.requires
user/eudet/hardware/CMakeFiles/eudet_static.dir/requires: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluController.cc.o.requires
user/eudet/hardware/CMakeFiles/eudet_static.dir/requires: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluHardware.cc.o.requires
user/eudet/hardware/CMakeFiles/eudet_static.dir/requires: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/FmctluI2c.cc.o.requires
user/eudet/hardware/CMakeFiles/eudet_static.dir/requires: user/eudet/hardware/CMakeFiles/eudet_static.dir/src/NiController.cc.o.requires

.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/requires

user/eudet/hardware/CMakeFiles/eudet_static.dir/clean:
	cd /home/calice/eudaq/user/eudet/hardware && $(CMAKE_COMMAND) -P CMakeFiles/eudet_static.dir/cmake_clean.cmake
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/clean

user/eudet/hardware/CMakeFiles/eudet_static.dir/depend:
	cd /home/calice/eudaq && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/calice/eudaq /home/calice/eudaq/user/eudet/hardware /home/calice/eudaq /home/calice/eudaq/user/eudet/hardware /home/calice/eudaq/user/eudet/hardware/CMakeFiles/eudet_static.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : user/eudet/hardware/CMakeFiles/eudet_static.dir/depend

