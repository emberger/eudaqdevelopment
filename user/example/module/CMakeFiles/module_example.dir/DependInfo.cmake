# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/calice/eudaq/user/example/module/src/Ex0Monitor.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0Monitor.cc.o"
  "/home/calice/eudaq/user/example/module/src/Ex0Producer.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0Producer.cc.o"
  "/home/calice/eudaq/user/example/module/src/Ex0RawEvent2StdEventConverter.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0RawEvent2StdEventConverter.cc.o"
  "/home/calice/eudaq/user/example/module/src/Ex0RunControl.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0RunControl.cc.o"
  "/home/calice/eudaq/user/example/module/src/Ex0TgDataCollector.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0TgDataCollector.cc.o"
  "/home/calice/eudaq/user/example/module/src/Ex0TgTsDataCollector.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0TgTsDataCollector.cc.o"
  "/home/calice/eudaq/user/example/module/src/Ex0TsDataCollector.cc" "/home/calice/eudaq/user/example/module/CMakeFiles/module_example.dir/src/Ex0TsDataCollector.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "EUDAQ_FUNC=__PRETTY_FUNCTION__ "
  "EUDAQ_PLATFORM=PF_LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "main/lib/core/include"
  "extern/include"
  "include"
  "main/lib/core/include/eudaq"
  "main/lib/lcio/include"
  "user/example/module/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/calice/eudaq/main/lib/core/CMakeFiles/core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
