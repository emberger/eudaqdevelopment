// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME ROOTProducer_ROOT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/home/calice/eudaq/user/itkstrip/itsroot/include/ROOTProducer.hh"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_ROOTProducer(void *p = 0);
   static void *newArray_ROOTProducer(Long_t size, void *p);
   static void delete_ROOTProducer(void *p);
   static void deleteArray_ROOTProducer(void *p);
   static void destruct_ROOTProducer(void *p);
   static void streamer_ROOTProducer(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOTProducer*)
   {
      ::ROOTProducer *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ROOTProducer >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ROOTProducer", ::ROOTProducer::Class_Version(), "ROOTProducer.hh", 15,
                  typeid(::ROOTProducer), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ROOTProducer::Dictionary, isa_proxy, 16,
                  sizeof(::ROOTProducer) );
      instance.SetNew(&new_ROOTProducer);
      instance.SetNewArray(&newArray_ROOTProducer);
      instance.SetDelete(&delete_ROOTProducer);
      instance.SetDeleteArray(&deleteArray_ROOTProducer);
      instance.SetDestructor(&destruct_ROOTProducer);
      instance.SetStreamerFunc(&streamer_ROOTProducer);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOTProducer*)
   {
      return GenerateInitInstanceLocal((::ROOTProducer*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOTProducer*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr ROOTProducer::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ROOTProducer::Class_Name()
{
   return "ROOTProducer";
}

//______________________________________________________________________________
const char *ROOTProducer::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ROOTProducer*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ROOTProducer::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ROOTProducer*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ROOTProducer::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ROOTProducer*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ROOTProducer::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ROOTProducer*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void ROOTProducer::Streamer(TBuffer &R__b)
{
   // Stream an object of class ROOTProducer.

   ::Error("ROOTProducer::Streamer", "version id <=0 in ClassDef, dummy Streamer() called"); if (R__b.IsReading()) { }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTProducer(void *p) {
      return  p ? new(p) ::ROOTProducer : new ::ROOTProducer;
   }
   static void *newArray_ROOTProducer(Long_t nElements, void *p) {
      return p ? new(p) ::ROOTProducer[nElements] : new ::ROOTProducer[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTProducer(void *p) {
      delete ((::ROOTProducer*)p);
   }
   static void deleteArray_ROOTProducer(void *p) {
      delete [] ((::ROOTProducer*)p);
   }
   static void destruct_ROOTProducer(void *p) {
      typedef ::ROOTProducer current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_ROOTProducer(TBuffer &buf, void *obj) {
      ((::ROOTProducer*)obj)->::ROOTProducer::Streamer(buf);
   }
} // end of namespace ROOT for class ::ROOTProducer

namespace {
  void TriggerDictionaryInitialization_libeudaq_ROOTProducer_ROOT_Impl() {
    static const char* headers[] = {
"include/ROOTProducer.hh",
0
    };
    static const char* includePaths[] = {
"/home/calice/eudaq/main/lib/core/include",
"/home/calice/eudaq/extern/include",
"/home/calice/eudaq/include",
"/home/calice/eudaq/main/lib/core/include/eudaq",
"/home/calice/eudaq/main/lib/lcio/include",
"/home/calice/root_6.12.06/include",
"/home/calice/eudaq/user/itkstrip/itsroot/.",
"/home/calice/eudaq/user/itkstrip/itsroot/include",
"/home/calice/eudaq",
"/home/calice/root_6.12.06/include",
"/home/calice/eudaq/user/itkstrip/itsroot/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libeudaq_ROOTProducer_ROOT dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$include/ROOTProducer.hh")))  ROOTProducer;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libeudaq_ROOTProducer_ROOT dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "include/ROOTProducer.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"ROOTProducer", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libeudaq_ROOTProducer_ROOT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libeudaq_ROOTProducer_ROOT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libeudaq_ROOTProducer_ROOT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libeudaq_ROOTProducer_ROOT() {
  TriggerDictionaryInitialization_libeudaq_ROOTProducer_ROOT_Impl();
}
